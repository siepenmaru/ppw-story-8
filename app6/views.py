from django.shortcuts import render, redirect
from .models import OpinionBoard
from .forms import OpinionBoardForm

# Create your views here.
def index(request):
    return render(request, "index.html")